// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCUG-yElu6s92MRnxglXCBrOdQwJWT_d7M",

    authDomain: "materialdeportivo-23332.firebaseapp.com",
  
    databaseURL: "https://materialdeportivo-23332-default-rtdb.europe-west1.firebasedatabase.app",
  
    projectId: "materialdeportivo-23332",
  
    storageBucket: "materialdeportivo-23332.appspot.com",
  
    messagingSenderId: "754567890286",
  
    appId: "1:754567890286:web:8ddd8e118acdf0f33d6d7f",
  
    measurementId: "G-53K66EZNDM"
  
  },
  mapBoxToken: "pk.eyJ1IjoiYXJuYXVtMTQiLCJhIjoiY2wzYm1pc3NmMDNmcDNsbW04YjY1bHIwcCJ9.S8t6e2ubOpxtUY3e9x35aw"

};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


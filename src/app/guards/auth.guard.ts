import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticateService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  // canLoad(
  //   route: Route,
  //   segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }
  constructor(public authService: AuthenticateService,
    public router: Router){ }
 
  canLoad(): boolean {
    const result = this.authService.isLoggedIn
    if(!result){
      this.router.navigateByUrl('/login');
    }
    return this.authService.isLoggedIn;
  }
 
}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  // {
  //   path: 'home',
  //   loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  // },
  {
    path: '',
    redirectTo: 'pantalla-carga',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'list-books',
    loadChildren: () => import('./pages/list-books/list-books.module').then( m => m.ListBooksPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'edit-book/:id',
    loadChildren: () => import('./pages/edit-book/edit-book.module').then( m => m.EditBookPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'create-book',
    loadChildren: () => import('./pages/create-book/create-book.module').then( m => m.CreateBookPageModule),
    canLoad: [AuthGuard]  
  },
  {
    path: 'pantalla-carga',
    loadChildren: () => import('./pages/pantalla-carga/pantalla-carga.module').then( m => m.PantallaCargaPageModule)
  },
  {
    path: 'list-users',
    loadChildren: () => import('./pages/list-users/list-users.module').then( m => m.ListUsersPageModule),
    canLoad: [AuthGuard]  
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
/*import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
 
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'create-book',
    loadChildren: () => import('./pages/create-book/create-book.module').then( m => m.CreateBookPageModule)
  },
  {
    path: 'list-books',
    loadChildren: () => import('./pages/list-books/list-books.module').then( m => m.ListBooksPageModule)
  },
  {
    path: 'edit-book/:id',
    loadChildren: () => import('./pages/edit-book/edit-book.module').then( m => m.EditBookPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }*/

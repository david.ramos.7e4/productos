import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Book } from '../app.module';
import { User } from '../app.module';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private firestore: AngularFirestore, private router: Router) { }

  createBook(book: Book){
    return this.firestore.collection('books').add(book);
  }
 
  getBooks() {
    return this.firestore.collection('books').snapshotChanges();
  }

  getUsers() {
    return this.firestore.collection('users').snapshotChanges();
  }
 
  getBook(id) {
    return this.firestore.collection('books').doc(id).valueChanges();
  }
 
  updateBook(id, book: Book) {
    this.firestore.collection('books').doc(id).update(book)
      .then(() => {
        this.router.navigate(['list-books']);
      }).catch(error => console.log(error));
  }
 
  deleteBook(id) {
    this.firestore.doc('books/'+id).delete();
  }
 
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pantalla-carga',
  templateUrl: './pantalla-carga.page.html',
  styleUrls: ['./pantalla-carga.page.scss'],
})
export class PantallaCargaPage implements OnInit {

  constructor(public router:Router) {
    setTimeout(()=>{
      this.router.navigateByUrl('login')
    }, 3000);
   }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/app.module';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.page.html',
  styleUrls: ['./list-users.page.scss'],
})
export class ListUsersPage implements OnInit {

  users = [];
  constructor(private dataService: DataService) {
    console.log(this.users)
   }
  ngOnInit() {
    this.dataService.getUsers().subscribe(
      res => {
        this.users = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data() as User
          };
        })
      }
    );
  }
  verLoc(id){
    
  }


}

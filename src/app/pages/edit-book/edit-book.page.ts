import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.page.html',
  styleUrls: ['./edit-book.page.scss'],
})
export class EditBookPage implements OnInit {

  editForm: FormGroup;
  id: String;
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
    private router: Router, public formBuilder: FormBuilder) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getBook(this.id).subscribe(
      res => {
        this.editForm = this.formBuilder.group({
          title: [res['title']],
          editorial: [res['editorial']]
        })
      });
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      title: [''],
      editorial: ['']
    })
  }

  onSubmit() {
    this.dataService.updateBook(this.id, this.editForm.value);
  }


}

import { Component, OnInit } from '@angular/core';
import {  FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { getStorage, ref, uploadString } from "firebase/storage";

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.page.html',
  styleUrls: ['./create-book.page.scss'],
})
export class CreateBookPage implements OnInit {

  bookForm: FormGroup;

 constructor(private dataService: DataService,
   public formBuilder: FormBuilder,   
   private router: Router) { }

 ngOnInit() {
   this.bookForm = this.formBuilder.group({
     title: [''],
     editorial: ['']
   })
 }

 onSubmit() {
  if (!this.bookForm.valid) {
    return false;
  } else {
    this.dataService.createBook(this.bookForm.value)
    .then(() => {
      this.bookForm.reset();
      this.router.navigate(['login']);
    }).catch((err) => {
      console.log(err)
    });
  }
}



}



import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/app.module';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.page.html',
  styleUrls: ['./list-books.page.scss'],
})
export class ListBooksPage implements OnInit {

  books = [];
  constructor(private dataService: DataService) { }
  ngOnInit() {
    this.dataService.getBooks().subscribe(
      res => {
        this.books = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data() as Book
          };
        })
      }
    );
  }
  deleteBook(id) {
    if (window.confirm('Do you want to delete this book?')) {
      this.dataService.deleteBook(id)
    }
  }



}
